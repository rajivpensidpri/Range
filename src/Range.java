
public class Range {
	public static int start ; 
	public static int end ; 
	
	
	
	public Range(int start, int end) {
		this.start = start ; 
		this.end = end ; 
	}
	
	public int first() {
		return start ; 
	}
	
	public  int last() {
		return end ; 
	}
	
	public static int size() {
		return (end-start+1) ; 
	}
	
	
	public void decrementLeftEnd(int k) {
		start -= k ; 
	}
	
	public void decrementRightEnd(int k) {
		if (end-k < start ) {
			return  ;
		}
		end -= k ; 
	}
	
	public void incrementRightEnd(int k) {
		end += k ; 
	}
	
	public void incrementLeftEnd(int k) {
		if (start+k > end) {
			return ; 
		}
		start += k ; 
	}
	
	public void changeStart(int st) {
		if (st > end) {
			System.out.println("Operation Not possible");
			return ; 
		}
		this.start = st ; 
	}
	
	
	public void changeEnd(int e) {
		if (e < start) {
			System.out.println("Operation Not possible");
			return ; 
		}
		this.end  = e ; 
	}
	
	
	public int middle(Range r) {
		return (this.start + r.size()/2) ; 
	}
	
	public boolean intersect(Range r) {
		if (start <= r.start) {
			return (r.start <= end) ; 
		}
		else {
			if (end <= r.start) {
				return (end <= r.start) ; 
			}
		}
		return false ; 
	}
	
	public Range intersection(Range r) {
		if (!this.intersect(r)) {
			return null ; 
		}
		return new Range(Math.max(this.start,r.start),Math.min(this.end,r.end)) ; 
	
	}
	
	public int kthElementFromStart(int k) {
		return start+k-1 ; 
	}
	
	public int kthElementFromEnd(int k) {
		return end-k+1 ; 
	}
	
	public boolean contains(int k) {
		if (k <= end && k >= start) {
			return true ; 
		}
		return false ; 
	}
	
	
	public void rightShift(int k) {
		start += k ; 
		end += k ; 
		
	}
	
	public void leftShift(int k) {
		start -= k ; 
		end -= k ; 
	}
	
	public void rightShift() {
		rightShift(1) ; 
	}
	
	public void leftShift() {
		leftShift(1) ; 
	}
	
	
	public void removeFirstK(int k) {
		if (start+k > end) {
			System.out.println("Operation Not possible!");
		}
		else {
			start += k ; 
		}
	}
	
	
	public void removeLastK(int k) {
		if (end-k < start) {
			System.out.println("Operation Not possible!"); 
		}
		else {
			end -= k ; 
		}
	}
	
	
	public int sum() {
		int ret = 0 ; 
		for(int i = start ; i <= end ; i++) {
			ret += i ; 
		}
		return ret ; 
	}
	
	public void increment(int k) {
		this.changeStart(this.start-k);
		this.changeEnd(this.end + k) ; 
	}
	
	public void decrement(int k) {
		if (this.start+k > this.end-k) {
			System.out.println("Operation not poissible!");
			return ; 
		}
		this.changeStart(this.start + k);
		this.changeEnd(this.end - k);
	}

}
